function [z,target] = generateNDDM(dist)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

disp('generating DDM data...')

if ~isfield(dist,'mu'); dist.mu = 1; end
if ~isfield(dist,'si'); dist.si = 1; end
% if ~isfield(dist,'N'); dist.N = length(dist.mu); end
if ~isfield(dist,'prior'); dist.prior = ones(1,dist.N)/dist.N; end
if ~isfield(dist,'ntrials'); dist.ntrials = 100000; end
if ~isfield(dist,'maxt'); dist.maxt = 100; end

if length(dist.mu)==1 
    dist.mu = dist.mu*[1 -1];
end

% randomly select targets
ntargets = dist.N;
ndims = dist.N;
target = randi(ntargets,[dist.ntrials 1]);

% parameters of DDM
k = -diff(dist.mu)/dist.si^2;
de = diff(dist.mu.^2)/(2*dist.si^2);
s0 = 0; % hack: (diff(log(dist.prior))-de)/k;

% generate evidence
z = nan(dist.maxt+2, ntargets, dist.ntrials);
for itrial = 1:dist.ntrials
    for idim = 1:ndims    
        s = [s0; normrnd(dist.mu(2),dist.si,[dist.maxt 1])];
        cums(:,idim) = [cumsum( k*s + de ); inf];
    end
    s = [s0; normrnd(dist.mu(1),dist.si,[dist.maxt 1])];
    cums(:,target(itrial)) = [cumsum( k*s + de ); inf];
    z(:,:,itrial) = cums - log( sum( exp(cums), 2) );
end
