function [xopt, yopt, vopt] = optSampling(func, rx, nxs, opt)
% Sampling optimisation algorithm
% func = reward function handle
% rx = variable range
% nt = max # timesteps

if findstr(path,'3.4-2013-11-11\util'); else run startup; end % ensure gpml on path

% Generate samples
d = size(rx,2);
x = repmat(rx(1,:)',1,nxs) + repmat(rx(2,:)'-rx(1,:)',1,nxs).*rand(d,nxs);

% sample reward function
y = nan(1,nxs); v = nan(2,nxs);
if nargout(func)==1; for k = 1:nxs; v(:,k) = func(x(:,k)); end
else for k = 1:nxs; [y(k), v(:,k)] = func(x(:,k)); end; end

% fit GP
hyp.meanfunc = @meanZero; hyp.mean = []; 
hyp.covfunc = @covSEiso; hyp.cov = log([1; 1]); 
hyp.likfunc = @likGauss; hyp.lik = log(1);
evalc('hyp = minimize(hyp,''gp'', -100, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x'', y'')');

% specify response function
rfunc = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', y', xsample');

% find global maximum of acquisition function
[xopt, yopt] = find_max(rx, rfunc);

% estimate other parameters
v1 = v(1,:); v2 = v(2,:);
vfunc1 = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', v1', xsample');
vfunc2 = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', v2', xsample');
vopt = [vfunc1(xopt); vfunc2(xopt)];

% plot results
if d==1 && opt.onplot
    plot_1D(rx, hyp, x, y, xopt, yopt)
end
if d==2 && opt.onplot
    plot_2D(rx, hyp, x, y, xopt, yopt)
end

end

function [xopt, yopt] = find_max(rx, func)

d = size(rx,2);
ntest = 1000^d;
xtest = repmat(rx(1,:)',1,ntest) + repmat(rx(2,:)'-rx(1,:)',1,ntest).*rand(d,ntest);
ytest = func(xtest);

[yopt, iopt] = max(ytest);
xopt = xtest(:,iopt);

end

function plot_1D(rx, hyp, x, y, xopt, yopt)

% resample GP
ntest = 10000;
d = size(rx,2);
xtest = repmat(rx(1,:)',1,ntest) + repmat(rx(2,:)'-rx(1,:)',1,ntest).*sort(rand(d,ntest));
[ytest, s2] = gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', y', xtest');

% plot sampled reward function
figure(2); clf; hold on
plot(x, y, '.k', 'markersize',1);
plot(xopt, yopt, 'xb','markersize',10)
plot(xtest', ytest, 'r');
fill([xtest';flipud(xtest')],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
    [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
xlabel('theta'); ylabel('reward');
axis([0 10 floor(min(ytest)) 0])

end

function plot_2D(rx, hyp, x, y, xopt, yopt)

% % resample GP
% ntest = 10000;
% d = size(rx,2);
% xtest = repmat(rx(1,:)',1,ntest) + repmat(rx(2,:)'-rx(1,:)',1,ntest).*sort(rand(d,ntest));
% [ytest, s2] = gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', y', xtest');

% plot sampled reward function
figure(2); clf; hold on
plot3(x(1,:), x(2,:), y, '.k', 'markersize',1);
plot3(xopt(1), xopt(2), yopt, 'xb','markersize',10)
% plot(xtest', ytest, 'r');
% fill([xtest';flipud(xtest')],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
%     [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
xlabel('theta1'); ylabel('theta2'); zlabel('reward');
ax = axis; axis([ax(1:4) floor(min(ytest)) 0])

end