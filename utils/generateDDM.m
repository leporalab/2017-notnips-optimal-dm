function [z,target] = generateDDM(ntrials, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
p.addOptional('mu', 1);
p.parse(varargin{:});
p = p.Results;



disp('generating DDM data...')

if ~isfield(dist,'mu'); dist.mu = 1; end
if ~isfield(dist,'si'); dist.si = 1; end
if ~isfield(dist,'N'); dist.N = length(dist.mu); end
if ~isfield(dist,'prior'); dist.prior = ones(1,dist.N)/dist.N; end
if ~isfield(dist,'nTrials'); dist.ntrials = 100000; end
if ~isfield(dist,'maxTime'); dist.maxt = 100; end

if length(dist.mu)==1 
    switch dist.N       
        case 2; dist.mu = dist.mu*[1 -1];
        otherwise; disp('N not in range'); return
    end
end

% randomly select targets
ntargets = length(dist.mu);
target = randi(ntargets,[dist.ntrials 1]);

% parameters of DDM
k = -diff(dist.mu)/dist.si^2;
de = diff(dist.mu.^2)/(2*dist.si^2);
s0 = (diff(log(dist.prior))-de)/k;

% generate evidence
z = nan(dist.maxt+2, ntargets, dist.ntrials);
for itrial = 1:dist.ntrials
    s = [s0; normrnd(dist.mu(target(itrial)),dist.si,[dist.maxt 1])];
    cums = [cumsum( k*s + de ); inf];
    z(:,:,itrial) = [cums -cums];
end
