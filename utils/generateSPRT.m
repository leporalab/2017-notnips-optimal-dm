function [z,target] = generateSPRT(dist)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

disp('generating SPRT data...')

if ~isfield(dist,'mu'); dist.mu = 1; end
if ~isfield(dist,'si'); dist.si = 1; end
if ~isfield(dist,'N'); dist.N = length(dist.mu); end
if ~isfield(dist,'prior'); dist.prior = ones(1,dist.N)/dist.N; end
if ~isfield(dist,'ntrials'); dist.ntrials = 100000; end
if ~isfield(dist,'maxt'); dist.maxt = 100; end

if length(dist.mu)==1 
    switch dist.N       
        case 2; dist.mu = dist.mu*[1 -1];
        otherwise; disp('N not in range'); return
    end
end

% randomly select targets
[ndims, ntargets] = size(dist.mu);
target = randi(ntargets,[dist.ntrials 1]);

% generate evidence
z = nan(dist.maxt+2, ntargets, dist.ntrials);
for itrial = 1:dist.ntrials
    s = normrnd(dist.mu(target(itrial)),dist.si,[dist.maxt 1]);
    logl = [log(dist.prior); log( normpdf(s,dist.mu,dist.si) )];
    cumdlogl = [cumsum( diff(logl,1,2) ); inf];
    z(:,:,itrial) = [-cumdlogl cumdlogl];
end
