function plotResults(result, plots, options)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

disp('generating plots...')

% # plots
nplots = length(plots.yvar);

% enables single input for xvar, yvar
if ~iscell(plots.xvar); xvar = cell(1,nplots); [xvar{:}] = deal(plots.xvar); plots.xvar = xvar; end
if ~iscell(plots.yvar); yvar = cell(1,nplots); [yvar{:}] = deal(plots.yvar); plots.yvar = yvar; end

% take label from var if not specified 
trunc = @(str) str(find(str(:)=='.')+1:end);
if ~isfield(plots,'xlab'); plots.xlab = cellfun(trunc,plots.xvar,'un',0); end
if ~isfield(plots,'ylab'); plots.ylab = cellfun(trunc,plots.yvar,'un',0); end

% default plot colour and no fit shown
if ~isfield(plots,'col'); plots.col = cell(1,nplots); plots.col(:) = {'.k'}; end
if ~isfield(options,'fit'); options.fit = 0; end 
if ~isfield(options,'std'); options.std = 1; end 

% enables single input for xlab, ylab, col
if ~iscell(plots.xlab); xlab = cell(1,nplots); [xlab{:}] = deal(plots.xlab); plots.xlab = xlab; end
if ~iscell(plots.ylab); ylab = cell(1,nplots); [ylab{:}] = deal(plots.ylab); plots.ylab = ylab; end
if ~iscell(plots.col); col = cell(1,nplots); [col{:}] = deal(plots.col); plots.col = col; end

% enables single input for rx, ry
if isfield(plots,'rx'); if size(plots.rx,2)==1; plots.rx = repmat(plots.rx,1,nplots); end; end
if isfield(plots,'ry'); if size(plots.ry,2)==1; plots.ry = repmat(plots.ry,1,nplots); end; end
   
% loop through plots
for iplot = 1:nplots
    eval(['x = ' plots.xvar{iplot} ';']); 
    eval(['y = ' plots.yvar{iplot} ';']);
    
    subplot(1,nplots,iplot); grid on; hold on; box on; 
    if plots.col{iplot}=='-' plot(x, y, plots.col{iplot}); else scatter(x, y, plots.col{iplot}); end
    xlabel(plots.xlab{iplot});
    ylabel(plots.ylab{iplot});
    if isfield(plots,'tit'); title(plots.tit{iplot}); end
    ax = axis; 
    if ~isfield(plots,'rx'); rx(:,iplot) = ax(1:2); else rx(:,iplot) = plots.rx(:,iplot); end
    if ~isfield(plots,'ry'); ry(:,iplot) = ax(3:4); else ry(:,iplot) = plots.ry(:,iplot); end
    axis([rx(:,iplot); ry(:,iplot)]')
       
    ht = text(mean(rx(:,iplot)),ry(2,iplot),{''});
    ht.HorizontalAlignment = 'center'; ht.VerticalAlignment = 'top';

    % optional show GP fit to points
    if options.fit
        if findstr(path,'3.4-2013-11-11\util'); else run startup; end % ensure gpml on path
        if ~isfield(options,'cov'); hyp.cov = [log(0.01); log(1)]; else hyp.cov = options.cov; end
        if ~isfield(options,'lik'); hyp.lik = log(1); else hyp.lik = options.lik; end
        if ~isfield(options,'n'); options.n = 100; end
        
        
        hyp = minimize(hyp, @gp, -options.n, @infExact, [], @covSEiso, @likGauss, x(:), y(:));
        xtest = linspace(rx(1,iplot),rx(2,iplot),100)';
        [ytest, s2] = gp(hyp, @infExact, [], @covSEiso, @likGauss, x(:), y(:), xtest);
        
        fill([xtest;flipud(xtest)],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
            [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
        if plots.col{iplot}=='-' plot(x, y, plots.col{iplot}); else scatter(x, y, plots.col{iplot}); end
        plot(xtest, ytest, 'r'); % ensure overlaid
        
       if options.std; ht.String{end+1} = ['standard deviation ' num2str(mean(sqrt(s2)),'%3.3f')]; end
    end
end

% optional figure title
if isfield(options,'title')
    axes('Position',[0 0 1 1],'Visible','off');
    h = text(0.5,1,options.title,'units','normalized');
    h.HorizontalAlignment = 'center'; h.VerticalAlignment = 'top';
end

% optional show simulation time
if isfield(result,'time')
    ht.String{end+1} = ['simulation time ' num2str(result.time,'%3.2f')];
end

% optional export figure
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 3*nplots 3])
if isfield(options,'name')
    eval(['export_fig ''' options.name '.png'' -r300 -painters']);
%     savefig(options.name)
end
