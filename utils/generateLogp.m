function [z,target] = generateLogp(dist)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

disp('generating logL data...')

if ~isfield(dist,'mu'); dist.mu = 1; end
if ~isfield(dist,'si'); dist.si = 1; end
if ~isfield(dist,'N'); dist.N = length(dist.mu); end
if ~isfield(dist,'prior'); dist.prior = ones(1,dist.N)/dist.N; end
if ~isfield(dist,'ntrials'); dist.ntrials = 100000; end
if ~isfield(dist,'maxt'); dist.maxt = 100; end

if length(dist.mu)==1 
    switch dist.N       
        case 2; dist.mu = dist.mu*[1 -1];
        case 3; dist.mu = dist.mu*[0 sqrt(3)/2 -sqrt(3)/2
                                   1   -1/2     -1/2];
        case 4; dist.mu = dist.mu*[ 0   0       sqrt(2/3) -sqrt(2/3)
                                    0 sqrt(8)/3 sqrt(2)/3  sqrt(2)/3  
                                    1   -1/3      -1/3       -1/3 ];
        case 5; dist.mu = dist.mu*[ 0   0           0        sqrt(10)/4   -sqrt(10)/4
                                    0   0        sqrt(30)/6  sqrt(30)/12  sqrt(30)/12
                                    0 sqrt(15)/4 sqrt(15)/2  -sqrt(15)/12 -sqrt(15)/12
                                    1   -1/4       -1/4       -1/4      -1/4 ];
        otherwise; disp('N not in range'); return
    end
end

% randomly select targets
[ndims, ntargets] = size(dist.mu);
target = randi(ntargets,[dist.ntrials 1]);

% generate evidence
z = nan(dist.maxt+2, ntargets, dist.ntrials);
for itrial = 1:dist.ntrials
    for idim = 1:ndims
        s = normrnd(dist.mu(idim,target(itrial)),dist.si,[dist.maxt 1]);
        logl(:,:,idim) = [log(dist.prior); log( normpdf(s,dist.mu(idim,:),dist.si) )];
    end
    cumlogl = [cumsum(sum(logl,3),1); -ones(1,ntargets)/eps];
    z(:,:,itrial) = cumlogl - log( sum( exp(cumlogl), 2) );
end
