clear all; clf; %close all
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

% functions to rotate 3D probabilities to 2D plane
proj2Dpoint = @(p1,p2,p3) cat(3, sqrt(1/2)*squeeze(p3-p1+1), sqrt(2/3)*squeeze(p2-p1/2+1/2-p3/2));
proj2D = @(p) squeeze( proj2Dpoint( p(:,1)', p(:,2)', p(:,3)') );

% functions to find edge of decision boundaries
edgeDec = @(pdec, d) circshift( [pdec 1-pdec 0; pdec 0 1-pdec], [1 d-1]);
lineDec = @(p, n) p(1,:) + linspace(0,1,n)'*diff(p);

% vertices of region
vertices3D = [1 0 0; 0 1 0; 0 0 1; 1 0 0];
vertices2D = proj2D( vertices3D );

% scaling function
% scale = @(p, alpha) 1 + alpha./abs( prod( log(p) , 2) );
scale = @(p, alpha) 1 + alpha .* prod(p, 2);

% decision boundaries
n = 10000;
alpha = -5;

% set up meshgrid of probabilities
ngrid = 10000;
[p(:,:,1), p(:,:,2)] = meshgrid( linspace( 0, 1, ngrid ) ); p(:,:,3) = 1 - sum(p,3);
p = reshape(p,[ngrid^2 3]);
p = p( p(:,3)>0 , : );

% find decision boundaries
pdec = 0.5 : 0.1 : 0.9; ndec = length(pdec);
% pdec = 0.4; ndec = length(pdec);
for idec = 1 : ndec
    for d = 1:3
        boundary3D = lineDec( edgeDec( pdec(idec), d ), n );
        boundary2D{idec, d} = proj2D( boundary3D );
    end
    
    logp = log(p);
    logpdec = log( pdec(idec) );
    isdec = 0.00001 > abs( logp' - scale(p, alpha)' .* ones(3,1) * logpdec );
    p2D{idec} = proj2D( p( any(isdec), : ) );
end

% plot distributions
figure(1); clf; hold on;
col = colormap(jet(ndec));
for idec = 1:ndec
    plot(p2D{idec}(:,1), p2D{idec}(:,2), '.', 'color',col(idec,:),'markersize',1); 
    for d = 1:3 
        plot(boundary2D{idec,d}(:,1), boundary2D{idec,d}(:,2),...
            '--', 'color',col(idec,:), 'linewidth', 1); 
    end
end
plot(vertices2D(:,1), vertices2D(:,2), 'k')
axis off
text(vertices2D(1,1), 0.66*vertices2D(2,2), ['\alpha=' int2str(alpha)],'fontsize',10)

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 2.5])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
