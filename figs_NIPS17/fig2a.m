clear all; close all; clc; dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];
% rng(1)
% 
% % distribution
% dist.generate = @generateLogp; 
% dist.mu = 1/2; dist.si = 1; dist.N = 2;
% dist.ntrials = 10000;
% dist.maxt = 100;
% 
% % reward function
% reward.fun = @rewardWald;
% reward.nsamples = 1;
% reward.wcs = 0.05;
% 
% % optimization method
% opt.rx = [-log(dist.N) -eps]'; 
% opt.nt = 1000;
% 
% %% Analyis
% 
% % generate distribution data
% [dist.z, dist.target] = dist.generate(dist);
% 
% % generate samples
% x = linspace(opt.rx(1), opt.rx(2), opt.nt); 
%     
% % sample thresholds
% for t = 1:opt.nt
%     [r(t), v(:,t)] = reward.fun(x(t), dist, reward.wcs, reward.nsamples);
% end
% 
% result.expx = exp(x);
% result.r = r;5
% result.e = v(1,:);
% result.rt = v(2,:);
% 
% % save results
% save([dirPath mfilename],'result')
load([dirPath mfilename],'result')

%% Plot results

plots.xvar = 'result.expx';
plots.yvar = {'result.r'};
plots.xlab = 'threshold, \Theta';
plots.ylab = {''};
plots.rx = [0.5; 1];
plots.ry = [-1.5; 0];
options.fit = 1; options.cov = [log(0.2); log(1)]; options.n = 50;
options.std = 0;

clf
plotResults(result, plots, options)
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 2.4])
ax = gca; 
ax.XTick = 0.5:0.1:1; 
plot([0.5 0.5],[-1.5 0],'k')
LineH = get(gca,'Children'); x = get(LineH(2), 'XData'); y = get(LineH(2), 'YData');
[~,imx] = max(y);
plot(x(imx)*[1 1],[-2 0],'--r')
text(x(imx-1),-0.85,{'optimal','threshold'},'fontsize',8,'color','r','horizontalalign','right')
grid off; box off;
ylabel('reward= -W e - c T   ', 'interpreter','tex')
title('Optimal threshold')

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 1.9])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
