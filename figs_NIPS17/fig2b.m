clear all; close all; clc; dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];
% rng(1)
% 
% % distribution
% dist.generate = @generateLogp; % 
% dist.mu = 1/2; dist.si = 1; dist.N = 2;
% dist.ntrials = 100000;
% dist.maxt = 100;
% 
% % parameters for reward function
% reward.fun = @rewardWald;
% reward.nsamples = 1;
% reward.wcs = 0.05;
% 
% % parameters for optimization
% opt.fun = @optReinforce;
% opt.rx = log([1/dist.N 1])';
% opt.nt = 20000;
% opt.options.onplot = 0;
% 
% %% Analyis
% 
% % generate distribution data
% [dist.z,dist.target] = dist.generate(dist);
% 
% nParallel = 10;
% parfor iParallel = 1:nParallel
%     fprintf('[%i]: \n', iParallel)
%         
%     % learn thresholds
%     f = @(x) reward.fun(x, dist, reward.wcs, reward.nsamples);
%     [xopt, fopt, vopt] = opt.fun(f, opt.rx, opt.nt, opt.options);
%     
%     % store results
%     x(iParallel, :) = xopt;
%     r(iParallel, :) = fopt;
%     e(iParallel, :) = vopt(1,:);
%     rt(iParallel, :) = vopt(2,:);  
% end
% 
% result.t = 1:opt.nt;
% result.x = x;
% result.r = r;
% result.e = e;
% result.rt = rt;
% 
% save([dirPath mfilename],'result','opt','nParallel')
load([dirPath mfilename],'result','opt','nParallel')

%% Plot results

% apply smoothing filter
span = 100;
for i = 1:nParallel
    result.expx(i,:) = smooth(exp(result.x(i,:)), span);
    result.r(i,:) = smooth(result.r(i,:), span);
    result.e(i,:) = smooth(result.e(i,:), span);
    result.rt(i,:) = smooth(result.rt(i,:), span);
end

plots.xvar = 'result.t';
plots.yvar = {'result.expx'};
plots.xlab = 'trial (1 sample)';
plots.ylab = {'threshold, \Theta'};
plots.rx = [0; opt.nt/10];
plots.ry = [0.5; 1.01];
plots.col = '-';
options.fit = 0; 

clf; 
plotResults(result, plots, options)
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 2.5])
ax = gca; ax.XTick = 0:500:10000; ax.YTick = 0.5:0.1:1;
grid off; box off;
title('REINFORCE')

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 1.95])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
