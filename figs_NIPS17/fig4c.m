clear all; close all; clc; %dbstop if error

if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

load([homePath 'results\3choice\mcs_wcs\mcs_wcs.mat'])

%% Plot results

% average results
nav = 1000; rav = (opt.nt-nav) : opt.nt;
result.expx = mean(exp(result.x(:,rav)),2);
result.r = mean(result.r(:,rav),2);
result.e = mean(result.v(:,1,rav),3);
result.rt = mean(result.v(:,2,rav),3);
result = rmfield(result,'time');

plots.xvar = {'result.rt'};
plots.yvar = {'result.e'};
plots.xlab = {'mean decision time'};
plots.ylab = {'error rate'};
plots.col = {'.w'};
plots.rx = [1; 10];
plots.ry = [0; 0.65];

options.fit = 0; options.n = 40; options.std = 0;

plotResults(result, plots, options)
title('Decision time vs accuracy','Fontsize',13)
scatter(result.rt, result.e, 5, result.expx, 'filled')
grid off; box off
set(gca,'xtick',1:10,'ytick',0:0.1:1)
hg = gca; hg.Position = hg.Position.*[1.3 1.7 0.75 0.9];
hc = colorbar; hc.Position = hc.Position.*[1.4 1 0.5 1];
hyl = ylabel(hc,'decision threshold, \Theta'); %hyl.Position = hyl.Position.*[2 1 1];
set(gca,'fontsize',11)

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.75 2.5])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
