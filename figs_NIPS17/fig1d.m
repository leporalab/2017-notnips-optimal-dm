clear all; clf; %close all
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];
rng(1);

nTrials = 5000;
maxt = 50;

% decision threshold
pdec = 0.85;
zdec = log(pdec/(1-pdec)); 

% parameters for distribution
dist{1}.mu = 1/2; dist{1}.si = 1; dist{1}.N = 2;
dist{1}.ntrials = nTrials;
dist{1}.maxt = maxt;
dist{2} = dist{1};

% generate distribution data
dist{1}.generate = @generateSPRT; % dist{1}.generate = @generateDDM; 
dist{2}.generate = @generateLogp;
for i = 1:2; [dist{i}.z, dist{i}.target] = dist{i}.generate(dist{i}); end

%% analysis

% decisions via threshold crossing
[r{1},r{2}] = deal( nan(1,nTrials) ); 
[v{1},v{2}] = deal( nan(2,nTrials) );
for itrial = 1 : nTrials
    [r{1}(itrial), v{1}(:,itrial)] = rewardWald(zdec, dist{1}, [],[], itrial);      % log LR
    [r{2}(itrial), v{2}(:,itrial)] = rewardWald(log(pdec), dist{2}, [],[], itrial); % log L
end
e{1} = v{1}(1,:); rt{1} = v{1}(2,:);
e{2} = v{2}(1,:); rt{2} = v{2}(2,:);
nfinish = sum(isfinite(e{1}));
mrt = nanmean(rt{1});

for itrial = 1 : nTrials
    dist{1}.z( v{1}(2,itrial) + 2:end, :, itrial) = nan; 
    dist{2}.z( v{2}(2,itrial) + 2:end, :, itrial) = nan; 
end

% ensure same distribution
dist{1}.z = dist{2}.z(:,2,:) - dist{2}.z(:,1,:);

%% plot results

rTrials = (1:20) + 40;

% plot distributions
figure(1); clf; box off; hold on

hh1 = histogram(rt{1}); hh1.FaceColor = [0 1 0];
ax = axis; 
hh2 = histogram(rt{1}(~~e{1})); hh2.FaceColor = [1 0 0];
plot(mrt*[1 1],[0 nTrials],':k')
text(mrt+0.25,ax(4)*0.73,{'mean'},'fontsize',8,'HorizontalAlignment','left')
text(mrt+0.25,ax(4)*0.67,{'decision time'},'fontsize',8,'HorizontalAlignment','left')
title('Decision time distribution      ','fontsize',11); 
ylabel('frequency (%)','fontsize',9)
xlabel('decision time (DT)')
axis([0.5 12.5 0 ax(4)*0.75])
set(gca,'fontsize',9,'ytick',0:nTrials/20:nTrials,'yticklabel',0:5:100,...
    'xtick',1:20,'xticklabel',{''});
box off; %axis off
hl = legend('correct','incorrect'); hl.Box = 'off'; hl.Location = 'east'; 
hl.Position = hl.Position + [0.04 0 0 0];

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.75 2.5])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
