clear all; clf; %close all
rng(1);

if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

nTrials = 2000;
tMax = 50;

% decision threshold
pdec = 0.66;

% parameters for distribution
dist.mu = 1/2; dist.si = 1; dist.N = 3;
dist.ntrials = nTrials;
dist.maxt = tMax;
dist.generate = @generateLogp;

%% analysis

% generate distribution
[dist.z, dist.target] = dist.generate(dist);

% decisions via threshold crossing
r = nan(1,nTrials); 
v = nan(2,nTrials);
for itrial = 1:nTrials
    [r(itrial), v(:,itrial)] = rewardWald(log(pdec), dist, [],[], itrial); % log L
end
e = v(1,:); rt = v(2,:);
nfinish = sum(isfinite(e));
mrt = nanmean(rt);

% no trajectory after decision
for itrial = 1:nTrials
    dist.z( v(2,itrial) + 2:end, :, itrial) = nan; 
end

%% plot results

rTrials = (1:20) + 40;

% transformation to rotate coordinates
rot = @(p1,p2,p3) cat(3, sqrt(1/2)*squeeze(p3-p1+1), sqrt(2/3)*squeeze(p2-p1/2+1/2-p3/2));

% triangle boundaries
vrot = squeeze(rot([0 0 1 0],[0 1 0 0],[1 0 0 1]));

% decision boundaries
if length(pdec)==1; pdec = pdec*ones(3,1); end
trot1 = squeeze( rot( [pdec(1) pdec(1)], [1-pdec(1) 0], [0 1-pdec(1)] ) );
trot2 = squeeze( rot( [1-pdec(2) 0], [pdec(2) pdec(2)],  [0 1-pdec(2)] ) );
trot3 = squeeze( rot( [1-pdec(3) 0], [0 1-pdec(3)], [pdec(3) pdec(3)] ) );

% trajectories
prot = rot(exp(dist.z(:,1,:)),exp(dist.z(:,2,:)),exp(dist.z(:,3,:)));

% plot distributions
figure(1); clf

hsp = subplot(131); hsp.Position = hsp.Position.*[1 1 1.1 1.1] - [0.06 0.02 0 0]; hold on
line([1 0],[0 1],[0 0],'color','k')
line([1 0],[0 0],[0 1],'color','k')
line([0 0],[1 0],[0 1],'color','k')
patch([pdec(1) pdec(1) 1],[1-pdec(1) 0 0],[0 1-pdec(1) 0],0.9*[1 1 1],'EdgeColor','none')
patch([1-pdec(1) 0 0],[pdec(1) pdec(1) 1],[0 1-pdec(1) 0],0.9*[1 1 1],'EdgeColor','none')
patch([1-pdec(1) 0 0],[0 1-pdec(1) 0],[pdec(1) pdec(1) 1],0.9*[1 1 1],'EdgeColor','none')
plot3(squeeze(exp(dist.z(:,1,rTrials))),squeeze(exp(dist.z(:,2,rTrials))),squeeze(exp(dist.z(:,3,rTrials))),'.-')
plot3([0 0 0 1],[1 1 0 0],[0 1 1 1],':k')
plot3([pdec(1) pdec(1)],[1 1/3],[0 0],':k')
plot3([1 1/3],[pdec(1) pdec(1)],[0 0],':k')
plot3([pdec(1) pdec(1)],[0 0],[1 1/3],':k')
plot3([1 1/3],[0 0],[pdec(1) pdec(1)],':k')
plot3([0 0],[pdec(1) pdec(1)],[1 1/3],':k')
plot3([0 0],[1 1/3],[pdec(1) pdec(1)],':k')
plot3([pdec(1) pdec(1)],[1-pdec(1) 0],[0 1-pdec(1)],'--k')
plot3([1-pdec(1) 0],[pdec(1) pdec(1)],[0 1-pdec(1)],'--k')
plot3([1-pdec(1) 0],[0 1-pdec(1)],[pdec(1) pdec(1)],'--k')
set(gca,'fontsize',7,'ztick',0:0.33:1,'zticklabel',{'0','1/3','\Theta','1'},...
    'xtick',0:0.33:1,'xticklabel',{'0','1/3','\Theta','1'},...
    'ytick',0:0.33:1,'yticklabel',{'0','1/3','\Theta','1'})
set(gca,'fontsize',7)
text(0.9,0.2,0.2,'choose H_0','horizontalalign','center','verticalalign','middle','fontsize',8,'rotation',-60)
text(0.1,0.1,0.9,'choose H_2','horizontalalign','center','verticalalign','middle','fontsize',8)
text(0.2,0.9,0.2,'choose H_1','horizontalalign','center','verticalalign','middle','fontsize',8,'rotation',60)

view([1,1,1])
hzl = zlabel('P(H_2|x)','fontsize',8); hzl.Position = hzl.Position + [-0.05 0 0];
hxl = xlabel('P(H_0|x)','fontsize',8); hxl.Position = hxl.Position + [-0.15 -0.15 0];
hyl = ylabel('P(H_1|x)','fontsize',8); hyl.Position = hyl.Position + [-0.15 -0.15 0];
plot3(1/3,1/3,1/3,'.k','markersize',5)

hsp = subplot(332); hsp.Position = hsp.Position.*[1 1 1 1.05] - [0 0.04 0 0]; hold on
patch([0 tMax tMax 0],[pdec(1)*[1 1] 0.99*[1 1]], 0.9*[1 1 1],'EdgeColor',0.9*[1 1 1],'EdgeAlpha',1)
plot(0:tMax+1,exp(squeeze(dist.z(:,3,rTrials))),'.-')
plot([0 tMax],pdec(1)*[1 1],'--k') 
plot([0,0],[0 1.05],'k')
title('Sequential Bayesian Inference        ','fontsize',12); 
set(gca,'fontsize',9,'ytick',0:0.33:1,'yticklabel',{'0','1/3','\theta','1'},...
    'xtick',1:20,'xticklabel',{''});
axis([0 12 0 1.05])
hyl = ylabel('P(H_2|x)','fontsize',9); hyl.Position = hyl.Position+[0.3 0 0];
text(10,0.9,'choose H_2','horizontalalign','center','verticalalign','middle','fontsize',8)

hsp = subplot(335); hsp.Position = hsp.Position.*[1 1 1 1.05] - [0 0.02 0 0]; hold on
patch([0 tMax tMax 0],[pdec(1)*[1 1] 0.99*[1 1]], 0.9*[1 1 1],'EdgeColor',0.9*[1 1 1],'EdgeAlpha',1)
plot(0:tMax+1,exp(squeeze(dist.z(:,2,rTrials))),'.-')
plot([0 tMax],pdec(2)*[1 1],'--k')
plot([0,0],[0 1.05],'k')
set(gca,'fontsize',9,'ytick',0:0.33:1,'yticklabel',{'0','1/3','\theta','1'},...
    'xtick',1:20,'xticklabel',{''});
axis([0 12 0 1.05])
hyl = ylabel('P(H_1|x)','fontsize',9); hyl.Position = hyl.Position+[0.3 0 0];
text(10,0.9,'choose H_1','horizontalalign','center','verticalalign','middle','fontsize',8)

hsp = subplot(338); hsp.Position = hsp.Position.*[1 1 1 1.05] - [0 0 0 0]; hold on
patch([0 tMax tMax 0],[pdec(1)*[1 1] 0.99*[1 1]], 0.9*[1 1 1],'EdgeColor',0.9*[1 1 1],'EdgeAlpha',1)
plot(0:tMax+1,exp(squeeze(dist.z(:,1,rTrials))),'.-')
plot([0 tMax],pdec(3)*[1 1],'--k')
plot([0,0],[0 1.05],'k')
set(gca,'fontsize',9,'ytick',0:0.33:1,'yticklabel',{'0','1/3','\theta','1'},...
    'xtick',1:20,'xticklabel',{''});
axis([0 12 0 1.05])
hxl = xlabel('time steps'); hxl.Position = hxl.Position+[0 0.1 0];
hyl = ylabel('P(H_0|x)','fontsize',9); hyl.Position = hyl.Position+[0.3 0 0];
annotation('arrow',[0.43 0.61],[0.079 0.079])
text(10,0.9,'choose H_0','horizontalalign','center','verticalalign','middle','fontsize',8)

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 7 2])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
