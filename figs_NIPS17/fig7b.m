clear all; close all; clc; %dbstop if error

if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

load([homePath 'results\3choice\mcs_wcs_scale\mcs_wcs_scale.mat'])

%% Plot results

nav = 1000; rav = (opt.nt-nav) : opt.nt;

nSeries = length(result);
for iSeries = 1:nSeries
    resultAll{iSeries}.r = mean(result{iSeries}.r(:,rav),2);
    resultAll{iSeries}.e = mean(result{iSeries}.v(:,1,rav),3);
    resultAll{iSeries}.rt = mean(result{iSeries}.v(:,2,rav),3);
    resultAll{iSeries}.vals = result{iSeries}.vals;
    resultAll{iSeries}.wcs = rSeries(iSeries)*ones(size(result{iSeries}.vals));
    resultAll{iSeries}.expx = mean(exp(result{iSeries}.x(:,rav)),2);
end

close all
figure; hold on

plots.xvar = {'result.vals'};
plots.yvar = {'result.r'};
plots.xlab = {'boundary curvature, \alpha'};
plots.ylab = {'reward rate'};
plots.col = {'.w'};
plots.rx = [-10; 10];
plots.ry = [-1; 0];
options.fit = 0  ; options.n = 40; options.std = 0;
    
for iSeries = [nSeries nSeries:-1:1]

    result = resultAll{iSeries};
   
    plotResults(result, plots, options)
    scatter(result.vals, result.r, 5, result.wcs, 'filled') 
    grid off; box off
    set(gca, 'xtick',-10:5:10, 'ytick',-1:0.2:0)
    title('Optimal reward')
%     hg = gca; hg.Position = hg.Position.*[1.1 1.8 0.8 0.9];
    
end

 set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.25 2.25])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);

 % linear regression analysis
 nhalf = floor(nParallel/2);
 rn = 1:nhalf; rp = nhalf + 1 + (1:nhalf);
 for iSeries = 1:nSeries
     grad = polyfit(resultAll{iSeries}.vals', resultAll{iSeries}.r, 1);
     m(iSeries) = grad(1);
     mn(iSeries) = mean( resultAll{iSeries}.r(:) );
     sd(iSeries) = std( resultAll{iSeries}.r );
     rg(iSeries) = max( resultAll{iSeries}.r ) - min( resultAll{iSeries}.r );

 end
 
 