clear all; close all; clc; dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

load([dirPath 'fig2d.mat'], 'result','opt','nParallel')

%% Plot results

% apply smoothing filter
span = 100;
for i = 1:nParallel
    result.expx(i,:) = smooth(exp(result.x(i,:)), span);
    result.r(i,:) = smooth(result.r(i,:), span);
    result.e(i,:) = smooth(result.e(i,:), span);
    result.rt(i,:) = smooth(result.rt(i,:), span);
end

plots.xvar = 'result.t';
plots.yvar = {'result.r'};
plots.xlab = 'trial (100 samples)';
plots.ylab = {'reward rate'};
plots.rx = [0; opt.nt/10];
plots.ry = [-1; 0];
plots.col = '-';
options.fit = 0; 

clf; 
plotResults(result, plots, options)
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 2.5])
ax = gca; ax.XTick = 0:500:10000; ax.YTick = -1:0.2:0;
grid off; box off;
title('REINFORCE (batch)')

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.5 1.95])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
