clear all; clf; %close all
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];
rng(1);

nTrials = 5000;
maxt = 50;

% decision threshold
pdec = 0.85;
zdec = log(pdec/(1-pdec)); 

% parameters for distribution
dist{1}.mu = 1/2; dist{1}.si = 1; dist{1}.N = 2;
dist{1}.ntrials = nTrials;
dist{1}.maxt = maxt;
dist{2} = dist{1};

% generate distribution data
dist{1}.generate = @generateSPRT; % dist{1}.generate = @generateDDM; 
dist{2}.generate = @generateLogp;
for i = 1:2; [dist{i}.z, dist{i}.target] = dist{i}.generate(dist{i}); end

%% analysis

% decisions via threshold crossing
[r{1},r{2}] = deal( nan(1,nTrials) ); 
[v{1},v{2}] = deal( nan(2,nTrials) );
for itrial = 1 : nTrials
    [r{1}(itrial), v{1}(:,itrial)] = rewardWald(zdec, dist{1}, [],[], itrial);      % log LR
    [r{2}(itrial), v{2}(:,itrial)] = rewardWald(log(pdec), dist{2}, [],[], itrial); % log L
end
e{1} = v{1}(1,:); rt{1} = v{1}(2,:);
e{2} = v{2}(1,:); rt{2} = v{2}(2,:);
nfinish = sum(isfinite(e{1}));
mrt = nanmean(rt{1});

for itrial = 1 : nTrials
    dist{1}.z( v{1}(2,itrial) + 2:end, :, itrial) = nan; 
    dist{2}.z( v{2}(2,itrial) + 2:end, :, itrial) = nan; 
end

% ensure same distribution
dist{1}.z = dist{2}.z(:,2,:) - dist{2}.z(:,1,:);

%% plot results

rTrials = (1:20) + 40;

% plot distributions
figure(1); clf

subplot(131); hold on;
my = 3.5;
patch([0 maxt maxt 0],[zdec*[1 1] my*[1 1]], 0.9*[1 1 1],'EdgeColor','none')
patch([0 maxt maxt 0],[-zdec*[1 1] -my*[1 1]], 0.9*[1 1 1],'EdgeColor','none')
plot([0 0],my*[-1 1],'k')
plot(0:maxt+1,squeeze(dist{1}.z(:,1,rTrials)),'.-')
plot([0 maxt],zdec*[1 1; -1 -1]','--k')
title('DDM/SPRT'); 
axis([0 12 my*[-1 1]]); 
ax = gca; ax.XAxisLocation = 'origin';
set(gca,'fontsize',9,'ytick',[-zdec,0,zdec],'yticklabel',{'-\theta','0','+\theta'},...
    'xtick',1:20,'xticklabel',{''});
text(7,3,'choose H_1','fontsize',8)
text(7,-3,'choose H_0','fontsize',8)
hyl = ylabel('evidence = log PR', 'fontsize',9);
hxl = xlabel('time steps'); hxl.Position = hxl.Position+[-3 -4.9 0];
annotation('arrow',[0.15 0.33],[0.07 0.07])

hsp = subplot(232); hold on; hsp.Position = hsp.Position - [0 0.04 0 0]; 
patch([0 maxt maxt 0],[pdec*[1 1] [1 1]], 0.9*[1 1 1],'EdgeColor','none')
plot(0:maxt+1,exp(squeeze(dist{2}.z(:,1,rTrials))),'.-')
plot([0 maxt],pdec*[1 1],'--k') 
title('Sequential Bayesian Inference          ','fontsize',12); 
axis([0 12 0 1.05])
set(gca,'fontsize',9,'ytick',0:0.5:1,'yticklabel',{'0','1/2','1'},...
    'xtick',1:20,'xticklabel',{''});
hyl = ylabel('P(H_1|x)','fontsize',9); hyl.Position = hyl.Position+[0.6 0 0];
text(8,pdec+0.1,'choose H_1','fontsize',8)

hsp = subplot(235); hold on; hsp.Position = hsp.Position - [0 -0.01 0 0]; 
patch([0.05 maxt maxt 0.05],[pdec*[1 1] [1 1]], 0.9*[1 1 1],'EdgeColor','none')
plot(0:maxt+1,exp(squeeze(dist{2}.z(:,2,rTrials))),'.-')
plot([0 maxt],pdec*[1 1],'--k')
hxl = xlabel('time steps'); %hxl.Position = hxl.Position+[0 -0.75 0];
axis([0 12 0 1.05])
set(gca,'fontsize',9,'ytick',0:0.5:1,'yticklabel',{'0','1/2','1'},...
    'xtick',1:12,'xticklabel',{''});
hyl = ylabel('P(H_0|x)','fontsize',9); hyl.Position = hyl.Position+[0.6 0 0];
text(8,pdec+0.1,'choose H_0','fontsize',8)
annotation('arrow',0.275+[0.15 0.33],[0.07 0.07])

if ~exist(dirPath); mkdir(dirPath); end
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 7 2])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
