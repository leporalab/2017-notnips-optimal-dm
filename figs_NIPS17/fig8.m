clear all; close all; clc; dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17/'];

% % distribution
% dist.generate = @generateLogp; % dist.generate = @generateSPRT; %@generateDDM;
% dist.mu = 1/2; dist.si = 1; dist.N = 3;
% dist.ntrials = 100000;
% dist.maxt = 100;
% 
% % reward function
% reward.fun = @rewardScaled;
% reward.nsamples = 10000;
% reward.wcs = 0.05;
% 
% % optimization method
% opt.rx = [-log(dist.N) -eps; -20 20]; 
% opt.nt = 100;
% 
% %% Analyis
% 
% % generate distribution data
% [dist.z,dist.target] = dist.generate(dist);
% 
% % generate samples
% [x1, x2] = meshgrid( linspace(0, 1, opt.nt) );
% x1 = opt.rx(1,1) + x1*diff(opt.rx(1,:));
% x2 = opt.rx(2,1) + x2*diff(opt.rx(2,:));
% 
% % sample rewards
% for i = 1:opt.nt
%     parfor j = 1:opt.nt
%         r(i,j) = reward.fun([x1(i,j) x2(i,j)], dist, reward.wcs, reward.nsamples);
%     end
% end
% 
% % save results
% save([dirPath mfilename],'x1','x2','r')
load([dirPath mfilename],'x1','x2','r')

figure(1); clf
r(r<-1) = nan;
mesh(exp(x1), x2, r);
axis square; 
axis([0.4 1 -20 20 -0.9 -0.4]);
set(gca,'xtick',0.4:0.1:1,'ytick',-20:10:20,'ztick',-1:0.1:0)
set(gca,'fontsize',8)
colormap jet(256);
hc = colorbar; hc.Position = hc.Position.*[1 1.5 0.25 0.8];
hyl = ylabel(hc,'reward, r','fontsize',9); hyl.Position = hyl.Position.*[2 1 1];
hx = xlabel('threshold, \Theta','fontsize',10,'rotation',20); 
hx.Position = hx.Position+[-0.08 2 0];
hy = ylabel('curvature, \alpha','fontsize',10,'rotation',-30); 
hy.Position = hy.Position+[0.08 -3 0];
zlabel('reward rate','fontsize',10)
hg = gca; hg.Position = hg.Position.*[0.5 1 1 1];

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 3.5 2.5])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);

