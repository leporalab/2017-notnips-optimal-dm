clear all; close all; clc; %dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/2choice/' mfilename '/'];

% sensitivity analysis
varParallel = 'reward.wcs'; % need to manually enter in code
rParallel = linspace(0, 0.2, 1001); rParallel(1) = []; nParallel = length(rParallel);

% distribution
dist.generate = @generateLogp; % dist.generate = @generateSPRT; %@generateDDM;
dist.mu = 1/2; dist.si = 1; dist.N = 2;
dist.ntrials = 1000;
dist.maxt = 100;

% reward function
reward.fun = @rewardWald;
reward.nsamples = 100;
reward.wcs = 0.1;

% optimization method
opt.fun = @optReinforce;
opt.rx = log([1/dist.N 1])'; % rx = repmat([0; 10],1,1);
opt.nt = 5000;
opt.options.onplot = 0;

%% Iterate sensitivity analysis

% generate distribution data
[dist.z, dist.target] = dist.generate(dist);

% timer
tic

% parallel loop
parfor iParallel = 1 : nParallel
    fprintf('%i: %s=%4.2f \n', iParallel, varParallel, rParallel(iParallel) )
    
    % set parametee
    wcs = rParallel(iParallel);
    
    % set reward function
    f = @(x) reward.fun(x, dist, wcs, reward.nsamples);
    
    % optimization
    [xopt, fopt, vopt] = opt.fun(f, opt.rx, opt.nt, opt.options);
    
    % store (time series)
    x(iParallel,:,:) = xopt;
    r(iParallel,:) = fopt;
    v(iParallel,:,:) = vopt;
end

% stop timer
time = toc;

% pack results
result.x = x;
result.r = r;
result.v = v;
result.vals = rParallel;
result.time = time/nParallel;

% store results
if ~exist(dirPath); mkdir(dirPath); end
save([dirPath mfilename], 'result', 'opt', 'reward', 'rParallel','nParallel')
load([dirPath mfilename], 'result', 'opt', 'reward', 'rParallel','nParallel')
 
%% Plot results

% average results
nav = 1000; rav = (opt.nt-nav) : opt.nt;
result.x = mean(result.x(:,1,rav),3); 
result.r = mean(result.r(:,rav),2); 
result.e = mean(result.v(:,1,rav),3); 
result.rt = mean(result.v(:,2,rav),3); 

% probability threshold
result.expx = exp(result.x);

plots.xvar = 'result.vals';
plots.yvar = {'result.expx','result.expx','result.r','result.e','result.rt'};
plots.rx = [min(rParallel) max(rParallel)]';
plots.ry = [exp(opt.rx(1)) exp(opt.rx(1)) -1 0 1; exp(opt.rx(2)) exp(opt.rx(2)) 0 1 10];
options.name = [dirPath mfilename];
options.title = ['@' func2str(reward.fun) ' (nsamples=' num2str(reward.nsamples) ') '...
    '@' func2str(opt.fun) ' (nt=' num2str(opt.nt) ') '];
options.fit = 1;

figure(1); clf; plotResults(result, plots, options)

plots.xvar = {'result.rt'};
plots.yvar = {'result.e'};
plots.rx = [1 10]';
plots.ry = [0 1]';
options.title = '';
options.name = [dirPath mfilename '_eRT'];
options.fit = 1;

figure(2); clf; plotResults(result, plots, options)
