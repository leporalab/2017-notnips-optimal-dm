clear all; close all; clc; %dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/2choice/' mfilename '/'];

% % sensitivity analysis 
% varParallel = 'reward.wcs'; % need to manually enter in code
% rParallel = linspace(0, 0.2, 1001); rParallel(1) = []; nParallel = length(rParallel);
% varSeries = 'reward.nsamples';
% rSeries = [1 5 10 50 100]; nSeries = length(rSeries);
% 
% % distribution
% dist.generate = @generateLogp; 
% dist.mu = 1/2; dist.si = 1; dist.N = 2; 
% dist.ntrials = 100000;
% dist.maxt = 100;
% 
% % reward function
% reward.fun = @rewardWald;
% reward.nsamples = 100; 
% reward.wcs = 0.1; 
% 
% % optimization method
% opt.fun = @optReinforce;
% opt.rx = [-log(dist.N) 0]';
% opt.nt = 5000;
% opt.options.onplot = 0;
% 
% %% Iterate sensitivity analysis
% 
% % series loop
% for iSeries = 1 : nSeries
% 
%     % define variable
%     eval([varSeries ' = rSeries(iSeries);']);
%     
%     % generate distribution data
%     [dist.z, dist.target] = dist.generate(dist);
%     
%     % timer
%     tic
%     
%     % parallel loop
%     parfor iParallel = 1 : nParallel
%         fprintf('[%i %i]: %s=%i %s=%4.2f \n', iSeries, iParallel, ...
%            varSeries, rSeries(iSeries), varParallel, rParallel(iParallel) )
%         
%         % set parametee
%         wcs = rParallel(iParallel);
%         
%         % set reward function
%         f = @(x) reward.fun(x, dist, wcs, reward.nsamples);
%         
%         % optimization
%         [xopt, fopt, vopt] = opt.fun(f, opt.rx, opt.nt, opt.options);
%         
%         % store (time series)
%         x(iParallel,:) = xopt;
%         r(iParallel,:) = fopt;
%         v(iParallel,:,:) = vopt;
%     end
%     
%     % stop timer
%     time = toc;
% 
%     % pack results
%     result{iSeries}.x = x;
%     result{iSeries}.r = r;
%     result{iSeries}.v = v;
%     result{iSeries}.vals = rParallel;
%     result{iSeries}.time = time/nParallel;
% end
% 
% % store results
% if ~exist(dirPath); mkdir(dirPath); end
% save([dirPath mfilename], 'result', 'opt', 'varSeries','rSeries','nSeries', 'rParallel','nParallel')
load([dirPath mfilename], 'result', 'opt', 'varSeries','rSeries','nSeries', 'rParallel','nParallel')

%% Plot results

% average results
nav = 1000; rav = (opt.nt-nav) : opt.nt;
for iSeries = 1 : nSeries
    result{iSeries}.expx = mean(exp(result{iSeries}.x(:,rav)),2);
    result{iSeries}.r = mean(result{iSeries}.r(:,rav),2);
    result{iSeries}.e = mean(result{iSeries}.v(:,1,rav),3);
    result{iSeries}.rt = mean(result{iSeries}.v(:,2,rav),3);
end

resultAll = result; clear result;

for iSeries = 1 : nSeries

    close all
    result = resultAll{iSeries};

    plots.xvar = 'result.vals';
    plots.yvar = {'result.expx','result.r','result.e','result.rt'};
    plots.rx = rParallel([1 nParallel])';
    plots.ry = [exp(opt.rx(1)) -1 0 1; exp(opt.rx(2)) 0 1 10];
    options.name = [dirPath mfilename '_' int2str(iSeries)];
    options.title = [varSeries '=' mat2str(rSeries(iSeries))];
    options.fit = 1;
    
    plotResults(result, plots, options)

    plots.xvar = {'result.rt'};
    plots.yvar = {'result.e'};
    plots.rx = [1 10]';
    plots.ry = [0 1]';
    options.title = [varSeries '=' mat2str(rSeries(iSeries))];
    options.name = [dirPath mfilename '_eRT_' int2str(iSeries)];
    options.fit = 1;
    
    plotResults(result, plots, options)

end
