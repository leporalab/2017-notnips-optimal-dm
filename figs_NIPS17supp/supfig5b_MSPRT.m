clear all; close all; clc; %dbstop if error

if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17supp/'];

load([homePath 'results\MSPRT\mcs_wcs\mcs_wcs.mat'])

%% Plot results

% average results
nav = 1000; rav = (opt.nt-nav) : opt.nt;
result.expx = mean(exp(result.x(:,rav)),2);
result.r = mean(result.r(:,rav),2);
result.e = mean(result.v(:,1,rav),3);
result.rt = mean(result.v(:,2,rav),3);
result = rmfield(result,'time');

plots.xvar = {'result.vals'};
plots.yvar = {'result.r'};
plots.xlab = {'accuracy/time cost, W/c'};
plots.ylab = {'reward rate'};
plots.col = {'.w'};
plots.rx = [0; 0.2];
plots.ry = [-1; 0];
options.fit = 0; options.n = 40; options.std = 0;

plotResults(result, plots, options)
scatter(result.vals, result.r, 5, result.vals, 'filled')
grid off; box off
set(gca, 'xtick',0:0.05:0.2,'ytick',-1:0.2:0)
title('Optimal reward')

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.25 2.25])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
