clear all; close all; clc; %dbstop if error

if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/figs_NIPS17supp/'];

load([homePath 'results\3choice\mcs_wcs\mcs_wcs.mat'])

%% Plot results

% average results
nav = 1000; rav = (opt.nt-nav) : opt.nt;

result.expx = mean(exp(result.x(:,rav)),2);
result.r = mean(result.r(:,rav),2);
result.e = mean(result.v(:,1,rav),3);
result.rt = mean(result.v(:,2,rav),3);
result = rmfield(result,'time');

plots.xvar = {'result.rt'};
plots.yvar = {'result.e'};
plots.xlab = {'decision time'};
plots.ylab = {'error rate'};
plots.col = {'.w'};
plots.rx = [1; 10];
plots.ry = [0; 1-1/3];
options.fit = 0; options.n = 40; options.std = 0;

plotResults(result, plots, options)
scatter(result.rt, result.e, 5, result.vals, 'filled')
grid off; box off
hg = gca; hg.Position = hg.Position.*[1.2 1.8 0.75 0.88];
hc = colorbar; hc.Position = hc.Position.*[1.4 1 0.5 1];
hyl = ylabel(hc,'accuracy/time cost, W/c'); %hyl.Position = hyl.Position.*[1.8 1 1];
title('Decision time vs accuracy')
set(gca,'xtick',1:10,'ytick',0:0.1:1)

set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 2.75 2.25])
% print([dirPath mfilename],'-dpng','-r300')
eval(['export_fig ''' dirPath mfilename '.png'' -r300 -painters']);
