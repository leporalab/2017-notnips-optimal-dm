clear all; close all; clc; %dbstop if error
if length(computer)==length('PCWIN64')
    if length(getenv('USERNAME')) == length('Sophie'); homePath = ['D:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; 
    else homePath = ['C:\Users\' getenv('USERNAME') '\Google Drive\Data - Decision making\']; end
elseif length(computer)==length('MACI64'); homePath = ['/Users/' getenv('USER') '/Google Drive File Stream/My Drive/Data - Decision making/']; end
dirPath = [homePath 'results/MSPRT/' mfilename '/'];
 
% sensitivity analysis
varParallel = 'x(2,:)'; % need to manually enter in code
rParallel = linspace(-10, 10, 51); nParallel = length(rParallel); 
varSeries = 'reward.wcs';
rSeries = 0.01:0.01:0.2; nSeries = length(rSeries);

% distribution
dist.generate = @generateNDDM; % dist.generate = @generateSPRT; %@generateDDM;
dist.mu = 1/2; dist.si = 1; dist.N = 3;
dist.ntrials = 1000;
dist.maxt = 100;

% reward function
reward.fun = @rewardScaled;
reward.nsamples = 100;
reward.wcs = 0.02;

% optimization method
opt.fun = @optReinforce;
opt.rx = [-2*log(dist.N) -eps]'; 
opt.nt = 5000; 
opt.options.onplot = 0;

%% Iterate sensitivity analysis

% generate distribution data
[dist.z, dist.target] = dist.generate(dist);

% series loop
for iSeries = 1 : nSeries

    % define variable
    eval([varSeries ' = rSeries(iSeries);']);
     
    % timer
    tic
    
    % parallel loop
    parfor iParallel = 1 : nParallel
        fprintf('[%i %i]: %s=%i %s=%4.2f \n', iSeries, iParallel, ...
           varSeries, rSeries(iSeries), varParallel, rParallel(iParallel) )
        
        % set parametee
        x2 = rParallel(iParallel);
        
        % set reward function
        f = @(x1) reward.fun([x1 x2], dist, reward.wcs, reward.nsamples);
        
        % optimization
        [xopt, fopt, vopt] = opt.fun(f, opt.rx, opt.nt, opt.options);
        
        % store (time series)
        x(iParallel,:,:) = xopt;
        r(iParallel,:) = fopt;
        v(iParallel,:,:) = vopt;
    end
        
    % stop timer
    time = toc;
    
    % pack results
    result{iSeries}.x = x;
    result{iSeries}.r = r;
    result{iSeries}.v = v;
    result{iSeries}.vals = rParallel;
    result{iSeries}.time = time/nParallel;

    % store results
    save(mfilename)
end

if ~exist(dirPath); mkdir(dirPath); end
save([dirPath mfilename], 'result', 'opt', 'varSeries','rSeries','nSeries', 'rParallel','nParallel')
load([dirPath mfilename], 'result', 'opt', 'varSeries','rSeries','nSeries', 'rParallel','nParallel')

%% Plot results

% average results
nav = 100; rav = (opt.nt-nav) : opt.nt;
for iSeries = 1 : nSeries
    result{iSeries}.expx = mean(exp(result{iSeries}.x(:,rav)),2);
    result{iSeries}.r = mean(result{iSeries}.r(:,rav),2);
    result{iSeries}.e = mean(result{iSeries}.v(:,1,rav),3);
    result{iSeries}.rt = mean(result{iSeries}.v(:,2,rav),3);
end

resultAll = result; clear result;

for iSeries = 1 : nSeries

    close all
    result = resultAll{iSeries};

    plots.xvar = 'result.vals';
    plots.yvar = {'result.expx','result.r','result.e','result.rt'};
    plots.rx = [min(rParallel) max(rParallel)]';
    plots.ry = [exp(opt.rx(1)) -1 0 1; exp(opt.rx(2)) 0 1 10];
    options.name = [dirPath 'run' int2str(iSeries)];
    options.title = [varSeries '=' mat2str(rSeries(iSeries))];
    options.fit = 0;
    
    plotResults(result, plots, options)

    plots.xvar = {'result.rt'};
    plots.yvar = {'result.e'};
    plots.rx = [1 10]';
    plots.ry = [0 1]';
    options.title = [varSeries '=' mat2str(rSeries(iSeries))];
    options.name = [dirPath 'eRT' int2str(iSeries)];
    options.fit = 0;
    
    plotResults(result, plots, options)

end
